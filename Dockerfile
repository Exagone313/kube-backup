FROM registry.ewd.app/helper/kubectl:1.24 as kubectl

FROM alpine

RUN apk update && \
  apk add --update \
    bash \
    easy-rsa \
    git \
    openssh-client \
    curl \
    ca-certificates \
    jq \
    python3 \
    py3-yaml \
    libstdc++ \
    gpgme \
    git-crypt \
    && \
  rm -rf /var/cache/apk/*

RUN adduser -h /backup -D backup

COPY --from=kubectl /bin/kubectl /usr/local/bin/kubectl

COPY entrypoint.sh /
USER backup
ENTRYPOINT ["/entrypoint.sh"]
